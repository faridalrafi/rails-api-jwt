Rails.application.routes.draw do
  post 'users/signin' => 'user_token#create'
  post 'users/signup' => 'users#create'
  get 'users', to: 'users#index'
  get 'users/:id', to: 'users#show'
  delete 'users/delete', to: 'users#destroy' 
  put 'users/update', to: 'users#update'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
 
root 'home#index'
get 'auth', to: 'home#auth'
end
